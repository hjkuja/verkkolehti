# Verkkolehti -projekti

Teimme yksinkertaisen uutissivuston web-ohjelmointi ja web-palvelinohjelmointikursseille (JAMK).

Tarkoituksena oli luoda sivu, jossa käyttäjä voi lukea ja hakea uutisia.

Työ oli osa kahta eri kurssia, joissa ei käyty oikeastaan yhtään projektissa käytettyjä asioita. Iso osa projektin työstöstä (n. 60h/henkilö) meni käytettyjen tekniikoiden ja kirjastojen opetteluun ja tutkimiseen.

[Lisätietoa projektista](docs/doccari.md)

## Mitä sivulla on?

* Etusivulla näkee useita uutisia aikajärjestyksessä ja uutisia voi hakea otsikon perusteella.
* Kirjautuminen, käyttäjätilin hallinta (salasanan ja sähköpostin vaihto)
    * Sessiopohjainen sisäänkirjaus
    * Salasana varmistetaan tietokannasta bcrypt-kirjastolla
* Uutisten lisäys ja poisto (kirjautuneena)
    * Uutisiin tulee automaattisesti aikaleima ja kirjoittajan nimi, kategorian voi valita itse
* Skaalautuu mobiilille hyvin

## Tekijät

* [Hannu Kujanpää](https://www.linkedin.com/in/hjkuja/)
* [Markus Haverinen](https://www.linkedin.com/in/markus-haverinen/)
* Juho Blomberg

# Kuvia

## Työpöytäversio

![Etusivu](pics/p1.png)
![Uutinen](pics/p2.png)
![Kirjautuminen](pics/p3.png)
![Käyttäjäsivu](pics/p4.png)
![Artikkelin luonti](pics/p5.png)
![Artikkeli-ikkuna kirjauduttua](pics/p6.png)

## Mobiiliversio

![Mobiili 1](pics/m1.jpg)
![Mobiili 2](pics/m2.jpg)

# Sivustolla käytettyjä tekniikoita ja kirjastoja

* Node.js
* Express
* React
* Passport
* Bcrypt
* Sequelize
* Axios
* Moment
* MySQL
* Täysi lista löytyy [package.json](package.json)-tiedostosta

## Sivujen rakenne

![Auth.js tiedoston rakenne](/img/speksi/Appjs.png)
![server.js tiedoston rakenne](/img/speksi/serverjs.png)
