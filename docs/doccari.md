# Valmis dokumentaatio

Teimme yksinkertaisen uutissivuston web-ohjelmointi ja web-palvelinohjelmointikursseille.

[Linkki sivulle](https://markushave.me/)

## Sivustolla käytettyjä tekniikoita ja kirjastoja

* Node.js
* Express
* React
* Passport
* Bcrypt
* Sequelize
* Axios
* Moment
* MySQL
* Täysi lista löytyy [package.json](../package.json)-tiedostosta

## Sivujen rakenne

![Auth.js tiedoston rakenne](../img/speksi/Appjs.png)
![server.js tiedoston rakenne](../img/speksi/serverjs.png)

## Tietokannan sisältö 

![Suunnitelma 3](../img/tietokanta_suunnitelma5.png)


## Sivujen sisältö

* Etusivu
    * Uutisia
    * ~~Haku~~
    * Kirjautuminen

* Uutinen
    * Otsikko
    * Teksti
    * Kirjoittaja
    * Julkaisu- ja ~~muokkaus~~aika
    * Kategoria
    * Haetaan tietokannasta
        * MySQL-tietokanta serverillä

* Adminsivu (kun on kirjautunut sisään)
    * Voi lisätä uutisia
    * Voi poistaa ~~ja muokata (omia)~~ uutisia

* ~~Kategoriasivu~~
    * ~~Voi selata uutisia kategorioittain~~


## Serveri

* Koko homma pyörii Googlen Cloudin Compute Enginen VM Instansissa
    * VM instanssin distro on CentOS 8
    * Palvelimessa on NGINX web-palvelin, jossa on konfattu reverse proxy 80 portista 8080 porttiin.
        * Palvelimelle on hommattu oma domain sekä SSL-sertifikaatti Let's Encryptillä
    * Käyössä on myös Gitlab-runneri, joka hoitaa koodin buildauksen ja kontituksen.
    * Tietokantana toimii MySQL

## Mitä puuttuu ja miksi?

* Hakua ei saatu toimimaan oikein ajoissa
    * Periaatteessa haku toimii, mutta ainoastaan osoitekentästä (esim. /articles/param/matti)

* Muokkaussivu uutisille
    * -> muokkausaikaa uutisille ei ole

* Kaikki kirjautuneet käyttäjät voivat poistaa kaikkia artikkeleita

## Työaika

* Noin 50-60 tuntia per henkilö
    * Noin puolet oli perehtymistä uusiin asioihin

## Itsearvio

* Hannu Kujanpää, M0374
    * Onnistuin oppimaan mielestäni todella paljon uusia ja kurssilla käymättömiä asioita
    * Onnistuin tekemään mielestäni toimivia sivuja ja ratkaisuja
    * Joitakin haluamiani asioita jäi puuttumaan ajanpuutteen vuoksi
    * Arvosanaehdotus 5 

* Markus Haverinen, L4837
    * Opin todella paljon uutta niin backendi kuin frontend puolelta. Nodejs ja react tulivat hyvin tutuksi. Aikaa meni paljon uuden oppimiseen.
    * Projektin alussa laitoin Googlen Cloudiin pystyyn palvelimen johon itse ohjelma sitten lopuksi tulisi. Hoidin myös GitLabiin CI/CD-ketjun.
    * Ajan loppumisen takia muutama ominaisuus jäi toteuttamatta.
    * Arvosanaehdotus 5

* Juho Blomberg, L4122
    * Aikaa meni paljon uuden asian oppimiseen ja sisäistämiseen ja osa asioista jäi vielä vähän hataralle pohjalle. Opin kuitenkin uutta asiaa Node.js:stä. Sisältöä en saanut niin paljoa tehtyä kuin muu ryhmä. Arvosanaehdotus 3

## Tekijät

* Hannu Kujanpää, M0374
* Markus Haverinen, L4837
* Juho Blomberg, L4122