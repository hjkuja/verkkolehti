# Suunnitelma

## Tietokannan sisältö 

### Ensimmäinen versio tietokannasta

![Suunnitelma 1](../img/tietokanta_suunnitelma.png)

### Toinen versio tietokannasta

![Suunnitelma 2](../img/tietokanta_suunnitelma3.png)

### Kolmas versio tietokannasta

![Suunnitelma 3](../img/tietokanta_suunnitelma5.png)


## Sivujen sisältö

* Etusivu
    * Uutisia
    * Haku
    * Kirjautuminen

* Uutinen
    * Otsikko
    * Teksti
    * Kirjoittaja
    * Julkaisu- ja muokkausaika
    * Kategoria

* Adminsivu (kirjoittajan sivu)
    * Voi lisätä uutisia
    * Voi poistaa ja muokata (omia) uutisia

* Kategoriasivu
    * Voi selata uutisia kategorioittain

## Uutiset

* Haetaan tietokannasta 
