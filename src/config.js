module.exports = {
    database: 'mydb',
    dbUser: 'root',
    dbPasswd: '',
    dbHost: '127.0.0.1',
    dbDialect: 'mysql',
    PORT: process.env.PORT || 8080
};
